FROM openresty/openresty:alpine-fat

RUN mkdir /lua-scripts
#COPY ./lua/scripts/jwt-lua-scripts /lua-scripts
ADD nginx-jwt /usr/local/openresty/lualib/
COPY ./nginx.conf /usr/local/openresty/nginx/conf


ENTRYPOINT ["/usr/local/openresty/bin/openresty", "-g", "daemon off;", "-c", "/nginx.conf"]
